﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Sistema_de_facturacion_y_ventas.Models;

namespace Sistema_de_facturacion_y_ventas.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(IServiceProvider serviceProvider)
        {
            CreateRoles(serviceProvider);
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        //creamos un metodo para gestionar los roles de los usuarios
        private async Task CreateRoles(IServiceProvider serviceProvider)
        {
            string mensaje;

            try
            {
                //aqui obtenemos servicios de nuestra clase rolemanager de nuestra bd para gestionarlos
                var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                //creamos otro servicio para gestionar la informacion de los usuarios
                var userManager = serviceProvider.GetRequiredService<UserManager<IdentityUser>>();
                //creamos un arreglo para proporcionar a la tabla roles, los siguientes roles:
                String[] rolesName = { "Admin", "Client", "Secretary", "Seller" };
                //recorre cada elemento que contiene el arreglo, o sea cada rol.
                foreach (var item in rolesName)
                {
                    //si existe el metodo espera mientras realiza la tarea que este ejecutando el role
                    var roleExist = await roleManager.RoleExistsAsync(item);
                    //si el rol no existe, insertara el rol en la tabla de base de datos
                    if (!roleExist)
                    {
                        //se le indica que espere mientra el metodo realiza la tarea y crea el rol sino existe en la bd
                        await roleManager.CreateAsync(new IdentityRole(item));
                    }
                }

                //colocando role a usuario ya creado
                var user = await userManager.FindByIdAsync("492a1b44-0035-4d92-a5a6-c883ef64b635");
                await userManager.AddToRoleAsync(user, "Client");
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
            }
        }
    }
}
