﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sistema_de_facturacion_y_ventas.Models
{
    public class LoginViewModels
    {
        //este atributo permite enlazar las propiedades de tipo modelo
        [BindProperty]

        //esta esto es un objeto de la clase input model, ya que en una pagina razor necesitamos utilizar este objeto input
        public InputModel Input { get; set; }

        //esto lo colocamos para mostrar los mensajes de errores al intentar iniciar sesion
        [TempData]
        public string ErrorMessage { get; set; }

        //estas propiedades que constribuyen con el diseno de nuestro login
        public class InputModel
        {
            //permite validar que el correo introducido sea correcto
            [Required(ErrorMessage = "<font color='red'> El campo de correo electrónico es obligatorio</font>")]
            [EmailAddress(ErrorMessage = "<font color='red'> El campo de correo electrónico no es una dirección valida</font>")]
            public string Email { get; set; }

            //permite validar que la contrasena introducido sea correcto
            [Required(ErrorMessage = "<font color='red'> El campo de contraseña es obligatorio</font>")]
            [DataType (DataType.Password)]
            //esta propiedad permite validad la cantidad minima de caracteres del campo contrasena
            [StringLength (100, ErrorMessage = "< font color = 'red' > El número de caracteres {0} debe ser al menos {2} </ font >", MinimumLength =6)]
            public string Password { get; set; }
        }
    }
}
